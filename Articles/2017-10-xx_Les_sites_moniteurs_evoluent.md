# Les sites moniteurs évoluent !

Le site des moniteurs `http://WWW.moniteurs.glenans.asso.fr` a été mis en place en 2011. Il incluait, outre les actualités du Conseil des Moniteurs et de l'Encadrement Bénévole (CDMEB), un ensemble de documents PDF de diverses thématiques (pédagogie, sécurité, environnement, etc) et les documents pour les formateurs.

Lors de la dernière AG de l'encadrement bénévole en décembre 2016, le CDMEB vous présentait une mise à jour majeure : l'ouverture de l'adresse `http://DOC.moniteurs.glenans.asso.fr` regroupant cette documentation par dossiers (les documents formateurs étant rangés à part).

(copie d'écran du FTP)

Après plusieurs séances de travail, et pour répondre aux besoins d'ergonomie et de facilité de recherche, le Groupe Documentalistes du CDMEB est heureux de vous annoncer que `http://doc.moniteurs.glenans.asso.fr` devient aujourd'hui un site à part entière, sur lequel vous pouvez naviguer au gré de vos envies pour trouver le document recherché (et bien d'autres !) à l'aide de menus, de catégories et de mots-clés.

En complément, le stockage des documents par dossiers est conservé. Vous retrouverez l'arborescence connue dans un nuage NextCloud, joignable sur `https://drive.cdmeb.org` (identifiant : `visiteur`, mot de passe : `Glenans1947`), qui sera synchronisable sur tous vos appareils avant vos navigations déconnectées !

En résumé, 3 adresses à retenir :

* www.moniteurs.glenans.asso.fr : Site des actualités du CDMEB
* doc.moniteurs.glenans.asso.fr : Site de la documentation gérée par le CDMEB
* drive.cdmeb.org (visiteur/Glenans1947) : Stockage des documents dans un nuage synchronisable

Un prochain article sera consacré au développement et au fonctionnement du site doc.moniteurs.glenans.asso.fr  : surveillez les nouvelles, les contributions sont ouvertes à tous !